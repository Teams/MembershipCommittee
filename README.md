Useful URLs
===

* Membership benefits: https://wiki.gnome.org/MembershipCommittee/MembershipBenefits
* Emeritus Membership: https://wiki.gnome.org/MembershipCommittee/EmeritusMembers

Apply for full or Emeritus membership at https://foundation.gnome.org/membership.
