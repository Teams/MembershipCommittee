### Personal details

Your full name, e-mail address are taken directly from your GitLab profile.
You will be responsible for deciding which username you want to pick for your GNOME
Account as soon as your Foundation Membership has been approved. Automation will
propose a set of usernames you can choose from and suggest next steps accordingly.

### Benefits

Please see [Membership Benefits](https://handbook.gnome.org/foundation/membership-benefits.html).

## Contributions

Becoming a Foundation member requires the applicant to have made a non-trivial
amount of contributions to any of the GNOME Project contribution categories including
code, translations, advocacy, infrastructure, outreach and documentation. This can
include contributions to the main GNOME project or GNOME Circle projects. Processing
will be much quicker if you provide links. Please tell us whether you were an existing
Foundation member or have done something relevant for the GNOME Foundation in the past.

## References

It's mandatory for your application to be accepted to quote (via @gitlab_username) existing
GNOME maintainers or contributors who can vouch for your contributions.
